-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        DUBURCQ
-- Caption:       GoodOne
-- Project:       Name of the project
-- Changed:       2021-12-20 09:39
-- Created:       2021-12-20 09:39
PRAGMA foreign_keys = OFF;

-- Schema: mydb
BEGIN;
CREATE TABLE "client"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "adresse" VARCHAR(145) NOT NULL
);
CREATE TABLE "dev"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "prenom" VARCHAR(45) NOT NULL,
  "niveau" VARCHAR(45) NOT NULL
);
CREATE TABLE "projet"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(145) NOT NULL,
  "content" LONGTEXT NOT NULL,
  "client_id" INTEGER NOT NULL,
  CONSTRAINT "fk_projet_client"
    FOREIGN KEY("client_id")
    REFERENCES "client"("id")
);
CREATE INDEX "projet.fk_projet_client_idx" ON "projet" ("client_id");
CREATE TABLE "dev_has_projet"(
  "dev_id" INTEGER,
  "projet_id" INTEGER NOT NULL,
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  CONSTRAINT "fk_dev_has_projet_dev1"
    FOREIGN KEY("dev_id")
    REFERENCES "dev"("id"),
  CONSTRAINT "fk_dev_has_projet_projet1"
    FOREIGN KEY("projet_id")
    REFERENCES "projet"("id")
);
CREATE INDEX "dev_has_projet.fk_dev_has_projet_projet1_idx" ON "dev_has_projet" ("projet_id");
CREATE INDEX "dev_has_projet.fk_dev_has_projet_dev1_idx" ON "dev_has_projet" ("dev_id");
COMMIT;