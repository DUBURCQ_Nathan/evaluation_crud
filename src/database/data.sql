INSERT INTO dev('nom', 'prenom', 'niveau') VALUES
('Ben Miloud', 'Malik', 'Sénior');

INSERT INTO dev('nom', 'prenom', 'niveau') VALUES
('Lefèvre', 'Julien', 'Sénior');

INSERT INTO dev('nom', 'prenom', 'niveau') VALUES
('Joubert', 'Camille', 'Junior');

INSERT INTO dev('nom', 'prenom', 'niveau') VALUES
('Héragemi', 'Yasmine', 'Tech-Lead');

INSERT INTO client('nom', 'adresse') VALUES
('Tisséo', '123 Rue du Retard, Lagrève 31000');

INSERT INTO client('nom', 'adresse') VALUES
('PSG', '456 Avenue de la Champions League, Paname 75000');

INSERT INTO client('nom', 'adresse') VALUES
('M2K', '789 Allée de la technologie, Lyon 69000');

INSERT INTO projet('title', 'content', 'client_id') VALUES 
('Application Mobile Tisséo', 'Lorem ipsum tralalere oulalah rololoh', 1);

INSERT INTO projet('title', 'content', 'client_id') VALUES
('Refonte du Site M2K', 'Lorem ipsum tralalere oulalah rololoh', 3);

INSERT INTO projet('title', 'content', 'client_id') VALUES
('Création Site Web PSG', 'Lorem ipsum tralalere oulalah rololoh', 2);